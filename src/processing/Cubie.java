package processing;

import processing.core.PApplet;
import processing.core.PMatrix3D;
import processing.core.PVector;

//Represents a single cube within a Rubik's cube.
class Cubie {

    private PApplet parent;
    PMatrix3D matrix;
    int x ;
    int y ;
    int z ;
    private Face[] faces = new Face[6];

    Cubie(PApplet parent, PMatrix3D matrix, int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.parent = parent;
        this.matrix = matrix;

        faces[0] = new Face(parent,new PVector(0,0,-1),parent.color(0,0,255));
        faces[1] = new Face(parent,new PVector(0,0,1),parent.color(0,255,0));
        faces[2] = new Face(parent,new PVector(0,1,0),parent.color(255,255,255));
        faces[3] = new Face(parent,new PVector(0,-1,0),parent.color(255,255,0));
        faces[4] = new Face(parent,new PVector(1,0,0),parent.color(255,150,0));
        faces[5] = new Face(parent,new PVector(-1,0,0),parent.color(255,0,0));
    }

    void update(int x, int y, int z) {
        matrix.reset();
        matrix.translate(x, y, z);
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //Displays the single Cubie
    void show() {
       // parent.fill(0);
       // parent.stroke(0);
       // parent.strokeWeight(0.1f);
        parent.pushMatrix();
        parent.applyMatrix(matrix);
        //parent.box(1);
        for (Face face: faces){
            face.show();
        }
        parent.popMatrix();
    }

    void turnFacesZ(int dir){
        for (Face f: faces){
            f.turnZ(dir * PApplet.HALF_PI);
        }
    }

    void turnFacesY(int dir){
        for (Face f: faces){
            f.turnY(dir * PApplet.HALF_PI);
        }
    }

    void turnFacesX(int dir){
        for (Face f: faces){
            f.turnX(dir * PApplet.HALF_PI);
        }
    }

}
