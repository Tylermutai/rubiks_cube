package processing;

import processing.core.PApplet;
import processing.core.PVector;

class Face {
    private PVector normal;
    private int color;
    private PApplet parent;

    Face(PApplet parent, PVector normal, int color) {
        this.normal = normal;
        this.color = color;
        this.parent = parent;

    }

    void show() {
        parent.pushMatrix();
        parent.rectMode(parent.CENTER);
        parent.translate(0.5f * normal.x, 0.5f * normal.y, 0.5f * normal.z);
        if (PApplet.abs(normal.x) > 0) {
            parent.rotateY(parent.HALF_PI);
        } else if (PApplet.abs(normal.y) > 0) {
            parent.rotateX(parent.HALF_PI);
        }

        // parent.noStroke();
        // parent.fill(0);


        parent.fill(color);
        parent.stroke(0);
        parent.strokeWeight(0.1f);
        parent.square(0, 0, 1);
        // parent.rect(0,0,1,1,1,1,1,1);
        parent.popMatrix();
    }

    void turnZ(float angle) {
        PVector v2 = new PVector();
        v2.x = PApplet.round(normal.x * PApplet.cos(angle) - normal.y * PApplet.sin(angle));
        v2.y = PApplet.round(normal.x * PApplet.sin(angle) - normal.y * PApplet.cos(angle));
        v2.z = normal.z;
        normal = v2;
    }

    void turnY(float angle) {
        PVector v2 = new PVector();
        v2.x = PApplet.round(normal.x * PApplet.cos(angle) - normal.z * PApplet.sin(angle));
        v2.z = PApplet.round(normal.x * PApplet.sin(angle) - normal.z * PApplet.cos(angle));
        v2.y = normal.y;
        normal = v2;
    }

    void turnX(float angle) {
        PVector v2 = new PVector();
        v2.y = PApplet.round(normal.y * PApplet.cos(angle) - normal.z * PApplet.sin(angle));
        v2.z = PApplet.round(normal.y * PApplet.sin(angle) - normal.z * PApplet.cos(angle));
        v2.x = normal.x;
        normal = v2;
    }
}
