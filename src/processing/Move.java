package processing;

import processing.core.PApplet;

class Move {
    int x;
    int y;
    int z;
    private int dir;
    private boolean animating = false;
    float angle = 0;
    private RubiksCube parent;

    Move(RubiksCube parent, int x, int y, int z, int dir){
        this.x = x;
        this.y = y;
        this.z = z;
        this.dir = dir;
        this.parent = parent;
    }

    void start(){
        animating = true;
    }

    void update(){
        if(animating) {
            angle += dir * 0.1;
            if (PApplet.abs(angle) > PApplet.HALF_PI) {
                angle = 0;
                animating = false;
                if(PApplet.abs(z) > 0) {
                    parent.turn(Axis.Z_AXIS, z, dir);
                } else if(PApplet.abs(y) > 0) {
                    parent.turn(Axis.Y_AXIS, y, dir);
                } else if(PApplet.abs(x) > 0) {
                    parent.turn(Axis.X_AXIS, x, dir);
                }
            }
        }
    }
}
