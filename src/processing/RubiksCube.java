package processing;

import peasy.PeasyCam;
import processing.core.PApplet;
import processing.core.PMatrix2D;
import processing.core.PMatrix3D;

import java.util.Date;

public class RubiksCube extends PApplet {
    private static int dim = 3; //Represents the structure of the Rubik's cube, i.e. 3*3 Rubik's cube.
    private Cubie[] cubies = new Cubie[dim * dim * dim];
    private Cubie[] originalCubies = new Cubie[dim * dim * dim];
    private Integer numberOfMoves = 100;
    private Character[] randomizedMoves = new Character[numberOfMoves];
    private Move[] moves = new Move[]{
            new Move(this, 0, 1, 0, 1),
            new Move(this, 0, 1, 0, -1),
            new Move(this, 0, -1, 0, 1),
            new Move(this, 0, -1, 0, -1),
            new Move(this, 1, 0, 0, 1),
            new Move(this, 1, 0, 0, -1),
            new Move(this, -1, 0, 0, 1),
            new Move(this, -1, 0, 0, -1),
            new Move(this, 0, 0, 1, 1),
            new Move(this, 0, 0, 1, -1),
            new Move(this, 0, 0, -1, 1),
            new Move(this, 0, 0, -1, -1)
    };
    private Move move = moves[0];
    private int counter = 0;
    private long intervals = 50;
    private boolean shuffling = false;
    private boolean solved = false;
    private long prevDate = new Date().getTime();
    private int secondsElapsed = 0;
    private int maxTime = 60;
    private String timeText = "0:0:" + secondsElapsed;
    private PeasyCam cam;
    private boolean startCounting = false;
    private int minutes = 0;
    private int hours = 0;

    @Override
    public void draw() {
        background(51);

        fill(255);
        textSize(32);
        text(counter, 100, 100);

        //Controls Instructions
        fill(255, 255, 0);
        textSize(12);
        text("Use the Shift key / Caps lock to Rotate face in the opposite direction", -400, -170);

        fill(255);
        textSize(12);
        text("Double Click Mouse - Reset Camera", -400, -150);

        fill(255);
        textSize(12);
        text("Mouse Wheel - Zoom in and out", -400, -130);

        fill(255);
        textSize(12);
        text("Mouse Drag - Rotate Camera", -400, -110);

        fill(255);
        textSize(12);
        text("ESCAPE - Exit", -400, -90);

        fill(255);
        textSize(12);
        text("q - Rotate Back Face", -400, -70);

        fill(255);
        textSize(12);
        text("w - Rotate Left Face", -400, -50);

        fill(255);
        textSize(12);
        text("e - Rotate Top Face", -400, -30);

        fill(255);
        textSize(12);
        text("z - Rotate Front Face", -400, -10);

        fill(255);
        textSize(12);
        text("x - Rotate Right Face", -400, 10);

        fill(255);
        textSize(12);
        text("c - Rotate Bottom Face", -400, 30);

        fill(0, 255, 0);
        textSize(12);
        text("1 - Shuffle Cube", -400, 50);

        fill(0, 255, 0);
        textSize(12);
        text("2 - Undo Shuffle", -400, 70);


        if (solved) {
            fill(0, 255, 0);
            textSize(32);
            text("Solved!", -60, -120);
            startCounting = false;
        }

        fill(255);
        textSize(32);
        if (startCounting) {
            if (!solved) {
                if (new Date().getTime() - prevDate >= 1000) {
                    minutes = (Integer.parseInt("" + timeText.charAt(timeText.indexOf(":") + 1)));
                    hours = Integer.parseInt("" + timeText.charAt(0));
                    if (secondsElapsed == maxTime) {
                        minutes++;
                        secondsElapsed = 0;
                        if (minutes == maxTime) {
                            hours++;
                            minutes = 0;
                        }
                    }
                    timeText = hours + ":" + minutes + ":" + secondsElapsed;
                    prevDate = new Date().getTime();
                    secondsElapsed++;
                }
            }
        }
        text("Time Elapsed: " + timeText, -150, -200);


        scale(50);
        rotateX(-0.5f);
        rotateY(0.4f);
        rotateZ(0.1f);
        for (Cubie cubie : cubies) {
            push();
            if (abs(cubie.z) > 0 && cubie.z == move.z) {
                rotateZ(move.angle);
            } else if (abs(cubie.x) > 0 && cubie.x == move.x) {
                rotateX(move.angle);
            } else if (abs(cubie.y) > 0 && cubie.y == move.y) {
                rotateY(-move.angle);
            }
            cubie.show();
            pop();
            if (shuffling)
                move.update();
        }
        if (!shuffling) move.update();


        //  if (frameCount % 50 == 0) {
        //    println("Draw(): " + Arrays.hashCode(cubies));
        // }
    }


    void turn(Axis axis, int index, int dir) {
        counter++;
        for (Cubie cubie : cubies) {
            int axisInteger;
            switch (axis) {
                case X_AXIS:
                    axisInteger = cubie.x;
                    break;
                case Y_AXIS:
                    axisInteger = cubie.y;
                    break;
                case Z_AXIS:
                    axisInteger = cubie.z;
                    break;
                default:
                    axisInteger = 0;
                    break;
            }
            if (axisInteger == index) {
                PMatrix2D matrix = new PMatrix2D();
                matrix.rotate(dir * HALF_PI);
                switch (axis) {
                    case X_AXIS:
                        matrix.translate(cubie.y, cubie.z);
                        cubie.update(cubie.x, round(matrix.m02), round(matrix.m12));
                        cubie.turnFacesX(dir);
                        break;
                    case Y_AXIS:
                        matrix.translate(cubie.x, cubie.z);
                        cubie.update(round(matrix.m02), cubie.y, round(matrix.m12));
                        cubie.turnFacesY(dir);
                        break;
                    case Z_AXIS:
                        matrix.translate(cubie.x, cubie.y);
                        cubie.update(round(matrix.m02), round(matrix.m12), round(cubie.z));
                        cubie.turnFacesZ(dir);
                        break;
                }

            }
        }
        solved = true;
        for (int i = 0; i < cubies.length; i++) {
            Cubie cubie = cubies[i];
            Cubie originalCubie = originalCubies[i];

            if (!(cubie.x == originalCubie.x && cubie.y == originalCubie.y && cubie.z == originalCubie.z)) {
                solved = false;
            }
        }

    }

    @Override
    public void settings() {
        //size(600, 600, P3D);
        fullScreen(P3D);
    }

    @Override
    public void keyPressed() {

        //Randomize cube if 1 is pressed:
        if (key == '1') {
            Runnable runnable = this::randomizeCube;
            Thread thread = new Thread(runnable);
            thread.start();
            return;
        }

        //Reverse moves if 2 is pressed:
        if (key == '2') {
            Runnable runnable = this::reverseMoves;
            Thread thread = new Thread(runnable);
            thread.start();
            return;
        }

        //Back Face
        if (key == 'q') {
            move = moves[10];
            move.start();
        } else if (key == 'Q') {
            move = moves[11];
            move.start();
        } else if (key == 'z') {
            move = moves[8];
            move.start();
        } else if (key == 'Z') {
            move = moves[9];
            move.start();
        }

        //Right Face
        else if (key == 'w') {
            move = moves[6];
            move.start();
        } else if (key == 'W') {
            move = moves[7];
            move.start();
        } else if (key == 'x') {
            move = moves[4];
            move.start();
        } else if (key == 'X') {
            move = moves[5];
            move.start();
        }

        //Top Face
        else if (key == 'e') {
            move = moves[2];
            move.start();
        } else if (key == 'E') {
            move = moves[3];
            move.start();
        } else if (key == 'c') {
            move = moves[0];
            move.start();
        } else if (key == 'C') {
            move = moves[1];
            move.start();
        }
    }

    //Randomize the cube

    private void randomizeCube() {
        counter=0;
        shuffling = true;
        Character[] possibleMoves = {'q', 'w', 'e', 'z', 'x', 'c'};
        Character[] randomizedMoves = new Character[numberOfMoves];

        for (int i = 0; i < randomizedMoves.length; i++) {
            randomizedMoves[i] = possibleMoves[parseInt(random(0, possibleMoves.length))];
            if (random(2) > 1) {
                randomizedMoves[i] = randomizedMoves[i].toString().toUpperCase().toCharArray()[0];
            } else {
                randomizedMoves[i] = randomizedMoves[i].toString().toLowerCase().toCharArray()[0];
            }
        }
        int count = 0;
        long prevTime = new Date().getTime();
        while (count < randomizedMoves.length) {
            long elapsedTime = new Date().getTime() - prevTime;
            if (elapsedTime > intervals) {
                key = randomizedMoves[count];
                keyPressed();
                count++;
                prevTime = new Date().getTime();
            } else {
                try {
                    Thread.sleep(intervals);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            this.randomizedMoves = randomizedMoves;
        }
        shuffling = false;
        startCounting = true;
        minutes=0;
        hours=0;
        secondsElapsed=0;
    }

    private void reverseMoves() {
        shuffling = true;
        if (!(randomizedMoves.length <= 0)) {
            long prevTime = new Date().getTime();
            int count = randomizedMoves.length - 1;
            while (count > -1) {
                long elapsedTime = new Date().getTime() - prevTime;
                if (elapsedTime > intervals) {
                    key = flipCase(randomizedMoves[count]);
                    keyPressed();
                    count--;
                    prevTime = new Date().getTime();
                } else {
                    try {
                        Thread.sleep(intervals);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        shuffling = false;
    }

    private Character flipCase(Character c) {
        if (c != null) {
            if (c.toString().toLowerCase().equals(c.toString())) {
                return c.toString().toUpperCase().toCharArray()[0];
            } else {
                return c.toString().toLowerCase().toCharArray()[0];
            }
        }
        return null;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void setup() {
        cam = new PeasyCam(this, 400);
        int index = 0;
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                for (int z = -1; z <= 1; z++) {
                    PMatrix3D matrix3D = new PMatrix3D();
                    matrix3D.translate(x, y, z);
                    cubies[index] = new Cubie(this, matrix3D, x, y, z);
                    index++;
                }
            }
        }
        int indexes = 0;
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                for (int z = -1; z <= 1; z++) {
                    PMatrix3D matrix3D = new PMatrix3D();
                    matrix3D.translate(x, y, z);
                    originalCubies[indexes] = new Cubie(this, matrix3D, x, y, z);
                    indexes++;
                }
            }
        }
    }
}
