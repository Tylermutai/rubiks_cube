package testing;

import processing.core.PApplet;

public class Drawing extends PApplet {
    private float circleX;
    private float circleY;
    @Override
    public void draw(){
        circleX = random(width);
        circleY = random(height);
        fill(round(random(255)),round(random(255)),round(random(255)));

        //line(pmouseX,pmouseY,mouseX,mouseY);
        ellipse(circleX,circleY,24,24);
    }

    @Override
    public void settings(){
        size(300,300);
    }

    @Override
    public void setup(){
        background(255);
        stroke(0,255,0);
    }
}
