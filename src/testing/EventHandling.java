package testing;

import processing.core.PApplet;

public class EventHandling extends PApplet {
    private float bx;
    private float by;
    private int boxSize = 75;
    private boolean overBox = false;
    private boolean locked = false;
    private double xOffset = 0.0;
    private double yOffset = 0.0;

    @Override
    public void draw() {
        background(237, 34, 93);

        // Test if the cursor is over the box
        if (
                mouseX > bx - boxSize &&
                        mouseX < bx + boxSize &&
                        mouseY > by - boxSize &&
                        mouseY < by + boxSize
        ) {
            overBox = true;
            if (!locked) {
                stroke(255);
                fill(244, 122, 158);
            }
        } else {
            stroke(156, 39, 176);
            fill(244, 122, 158);
            overBox = false;
        }

        // Draw the box
        rect(bx, by, boxSize, boxSize);
    }

    @Override
    public void settings() {
        size(720, 400);
    }

    @Override
    public void setup() {
        bx = width / 2.0f;
        by = height / 2.0f;
        rectMode(RADIUS);
        strokeWeight(2);
    }


    @Override
    public void mousePressed() {
        if (overBox) {
            locked = true;
            fill(255, 255, 255);
        } else {
            locked = false;
        }
        xOffset = mouseX - bx;
        yOffset = mouseY - by;
    }

    @Override
    public void mouseDragged() {
        if (locked) {
            bx = parseFloat("" + (mouseX - xOffset));
            by = parseFloat("" + (mouseY - yOffset));
        }
    }

    @Override
    public void mouseReleased() {
        locked = false;
    }

    public static void main(String[] args){
        EventHandling.main("testing.EventHandling");
    }
}
