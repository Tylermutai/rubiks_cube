package testing;

import processing.core.PApplet;

public class OriginalRubiksCube extends PApplet {
    private int startX = 100;
    private int startY = 100;
    private int rectSize = 10;
    private Squares[] squares = new Squares[9];

    @Override
    public void draw() {
        background(255);
        stroke(0);
        strokeWeight(3);
        rectMode(RADIUS);
        for (Squares square : squares)
            rect(square.x, square.y, rectSize, rectSize);
    }

    @Override
    public void settings() {
        size(600, 600, P3D);
    }

    @Override
    public void setup() {
        for (int i = 0; i < squares.length; i++) {
            if(i%3 == 0){
                startY += 20;
                startX = 100;
            }
            squares[i] = new Squares(startX, startY);
            startX += 20;
        }
    }

    public static void main(String[] args) {
        OriginalRubiksCube.main("testing.OriginalRubiksCube");
    }
}
