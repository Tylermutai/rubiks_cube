package testing;

import processing.core.PApplet;

import java.util.Date;

public class Window extends PApplet {
    private final long secondIntervals = 1000;
    private long prevTime =  new Date().getTime();
    private float finalX = 100;
    private float finalY = 10;
    private float initX = 0;
    private float initY = 10;
    @Override
    public void draw(){
        if(new Date().getTime() - prevTime > secondIntervals){
            initX += 10;
            finalX += 10;
            prevTime = new Date().getTime();
            //System.out.println("Expanding..." + " rectHeight: " + rectHeight + " rectWidth: " + rectWidth);
        }
        if(initX >= 300 || initY >= 300 || finalX >= 300 || finalY >= 300){
            initX = 0;
            finalX = 100;
        }background(255);
        line(initX,initY,finalX,finalY);
    }

    @Override
    public void settings(){
        size(300,300);
    }

    @Override
    public void setup(){
        stroke(0,255,0);
        line(0,10,100,10);
    }

}
